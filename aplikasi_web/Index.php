<?php
 session_start();
	include 'koneksi.php';
 ?>
 <!DOCTYPE html>
 <html>
 <head>
 	<title>Halaman Daftar Mahasiswa.com</title>
 	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
 	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.min.css">
 </head>
 <body bgcolor="red">

	 <!--navbar -->
	 
<?php include 'menu.php' ?>
 		<!--konten -->
 		<section class="konten">
 			<div class="container text-center">
 				<h1>DAFTAR MAHASISWA</h1>

 				<div class="row">

 					<?php $ambil = $koneksi->query("SELECT a.nim, a.nama ,b.nama_prodi ,a.photos FROM mahasiswa a , prodi b  WHERE a.`id_prodi`= b.`id_prodi` ORDER BY b.`nama_prodi`");?>
 					<?php while($mhs = $ambil->fetch_assoc()){?>
 						<div class="col-md-3">
 							<div class="thumbnail">
 								<img src="../images/<?php echo $mhs['photos']; ?>" alt="">
 								<div class="caption">
 									<h4 align="center">nama : <?php echo $mhs['nama']; ?></h3>
 									<h5 align="center">Nim  :  <?php echo $mhs['nim']; ?></h5>
                                     <h5 align="center">Prodi : <?php echo $mhs['nama_prodi']; ?></h5>
 									<a class="btn btn-danger center-block" href="hapus.php?nim=<?php echo $mhs['nim']?>">Hapus</a>
 								</div>
 							</div>
 						</div>		
 					<?php } ?>

 				</div>
 			</div>
 		</section>
 		<footer>
 			<p class="text-center">Polinema Kediri </p>
 		</footer>
 	</body>
 	</html>